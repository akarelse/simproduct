<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a simproduct module for PyroCMS
 *
 * @author 		Aat Karelse
 * @website		none
 * @package 	???
 * @subpackage 	simproduct Module
 */
class SimProduct_m extends MY_Model {

	public function __construct()
	{		
		parent::__construct();
		
		/**
		 * If the sample module's table was named "samples"
		 * then MY_Model would find it automatically. Since
		 * I named it "sample" then we just set the name here.
		 */
		$this->_table = 'sim_product';
	}
	
	
	public function get($id)
	{
		$this->db->select('f.name as fname,f.path as path, s.*')
    	->from('sim_product s' , 'files f')
    	->join('files f', 's.image_file_id = f.id', 'left')
    	->where('s.id', $id);
		return $this->db->get()->row();
	}
	
	
	public function get_all_products()
  	{

    	$this->db->select('f.name as fname,f.path as path,f.id as fid,f.description as description, s.*')
    		->from('sim_product s' , 'files f')
    		->join('files f', 's.image_file_id = f.id', 'left'); 
    return parent::get_all();
  	}
  	
  	public function get_all_images()
  	{
		$query = $this->db->select('name,id');
    	
    return $this->db->get('files')->result();
	}
	
	

	//make sure the slug is valid
	public function _check_slug($slug)
	{
		$slug = strtolower($slug);
		$slug = preg_replace('/\s+/', '-', $slug);

		return $slug;
	}
	
		//create a new item
	public function create($input)
	{
		if (!isset($input['price']))
		$input['price'] = 0;
		elseif ($input['price'] == '')
		$input['price'] = 0;
		
		
		$to_insert = array(
			'name' => $input['name'],
			'body' => $input['body'],
			'image_file_id' => $input['image_file_id'],
			'price' => $input['price'],
			
		);

		return $this->db->insert('sim_product', $to_insert);
	}
	
	public function get_filenames()
	{
		$this->db->select('sim_product.image_file_id, files.name')
						->distinct()
						->join('files', 'sim_product.image_file_id = files.id', 'left');
		$filenames = parent::get_all();

		$options = array();
		
		if ( ! empty($filenames))
		{
			foreach($filenames as $filename)
			{
				
			}
		}

		asort($options);

		return $options;
	}
}
