<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_SimProduct extends Module {

	public $version = '2.1';

	public function info()
	{
		return array(
			'name' => array(
				'en' => 'Simple product'
			),
			'description' => array(
				'en' => 'This is a simple product module for pyrocms.'
			),
			'frontend' => TRUE,
			'backend' => TRUE,
			'menu' => 'content', // You can also place modules in their top level menu. For example try: 'menu' => 'Sample',
			'sections' => array(
				'items' => array(
					'name' 	=> 'simproduct:items', // These are translated from your language file
					'uri' 	=> 'admin/simproduct',
						'shortcuts' => array(
							'create' => array(
								'name' 	=> 'simproduct:create',
								'uri' 	=> 'admin/simproduct/create',
								'class' => 'add'
								)
							)
						)
				)
		);
	}

	public function install()
	{
		$this->dbforge->drop_table('sim_product');

		$sample = array(
			'sim_product' => array(
                        'id' => array(
									  'type' => 'INT',
									  'constraint' => '11',
									  'auto_increment' => TRUE,
									  'primary' => true
									  ),
						'name' => array(
										'type' => 'VARCHAR',
										'constraint' => '100'
										),
						'body' => array(
										'type' => 'VARCHAR',
										'constraint' => '512'
										),
						'image_file_id' => array(
										'type' => 'INT',
										'constraint' => '1',
										'default'=> '0'
										),
						'price' => array(
										'type' => 'INT',
										'constraint' => '1',
										'default'=> '0'
										)
								)
						);



		if(	 $this->install_tables($sample)  AND
		is_dir($this->upload_path.'simproduct') OR @mkdir($this->upload_path.'simproduct',0777,TRUE))
		{
			return TRUE;
		}


	}

	public function uninstall()
	{
		$this->dbforge->drop_table('sim_product');
		{
			return TRUE;
		}
	}


	public function upgrade($old_version)
	{
		// Your Upgrade Logic
		return TRUE;
	}

	public function help()
	{
		// Return a string containing help info
		// You could include a file and return it here.
		return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
	}
}
/* End of file details.php */
