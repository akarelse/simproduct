<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a simproduct module for PyroCMS
 *
 * @author 		Aat Karelse
 * @website		none yet
 * @package 	???
 * @subpackage 	simproduct Module
 */
class Admin extends Admin_Controller
{
	protected $section = 'items';

	public function __construct()
	{
		parent::__construct();

		// Load all the required classes
		$this->load->model('simproduct_m');
		$this->load->library('form_validation');
		$this->lang->load('simproduct');

		// Set the validation rules
		$this->item_validation_rules = array(
			array(
				'field' => 'name',
				'label' => 'Name',
				'rules' => 'trim|max_length[20]|required'
			),
			array(
				'field' => 'body',
				'label' => 'body',
				'rules' => 'trim|max_length[100]|required'
			),
			array(
				'field' => 'image',
				'label' => 'image',
				'rules' => 'integer|trim|max_length[3]'
			),
			array(
				'field' => 'price',
				'label' => 'price',
				'rules' => 'integer|trim|max_length[10]'
			)
			
		);

		// We'll set the partials and metadata here since they're used everywhere
		$this->template->append_js('module::admin.js')
						->append_css('module::admin.css')
						->append_metadata($this->load->view('fragments/wysiwyg', $this->data, TRUE));
	}

	/**
	 * List all items
	 */
	public function index()
	{
		// here we use MY_Model's get_all() method to fetch everything
		$items = $this->simproduct_m->get_all_products();

		// Build the view with simproduct/views/admin/items.php
		$this->data->items =& $items;
		$this->template->title($this->module_details['name'])
						->build('admin/items', $this->data);
	}

	public function create()
	{
		// Set the validation rules from the array above
		$this->form_validation->set_rules($this->item_validation_rules);
		
		//filling of image list
		//print_r($this->simproduct_m->get_all_images());
		
		$_images = array('-1'=>'none');
		if ($images = $this->simproduct_m->get_all_images())
		{
			foreach ($images as $image)
			{
				$_images[$image->id] = $image->name;
			}
		}	
		$this->data->images =&  $_images;
		//$this->data->image_file_id = -1;
		//print_r($this->db->last_query());
		
		// check if the form validation passed
		if($this->form_validation->run())
		{
			// See if the model can create the record
			if($this->simproduct_m->create($this->input->post()))
			{
				// All good...
				$this->session->set_flashdata('success', lang('simproduct.success'));
				redirect('admin/simproduct');
			}
			// Something went wrong. Show them an error
			else
			{
				$this->session->set_flashdata('error', lang('simproduct.error'));
				redirect('admin/simproduct/create');
			}
		}
		
		
		
		foreach ($this->item_validation_rules AS $rule)
		{
			$this->data->{$rule['field']} = $this->input->post($rule['field']);
		}

		// Build the view using simproduct/views/admin/form.php
		$this->template->title($this->module_details['name'], lang('simproduct.new_item'))
						->build('admin/form', $this->data);
	}
	
	public function edit($id = 0)
	{
		$this->data = $this->simproduct_m->get($id);
		

		// Set the validation rules from the array above
		$this->form_validation->set_rules($this->item_validation_rules);
		
		//filling of image list
		$_images = array();
		if ($images = $this->simproduct_m->get_all_images())
		{
			foreach ($images as $image)
			{
				$_images[$image->id] = $image->name;
			}
		}	
		$this->data->images =&  $_images;

		// check if the form validation passed
		if($this->form_validation->run())
		{
			// get rid of the btnAction item that tells us which button was clicked.
			// If we don't unset it MY_Model will try to insert it
			unset($_POST['btnAction']);
			
			// See if the model can create the record
			if($this->simproduct_m->update($id, $this->input->post()))
			{
				// All good...
				$this->session->set_flashdata('success', lang('simproduct.success'));
				redirect('admin/simproduct');
			}
			// Something went wrong. Show them an error
			else
			{
				$this->session->set_flashdata('error', lang('simproduct.error'));
				redirect('admin/simproduct/create');
			}
		}

		// Build the view using simproduct/views/admin/form.php
		$this->template->title($this->module_details['name'], lang('simproduct.edit'))
						->build('admin/form', $this->data);
	}
	
	public function delete($id = 0)
	{
		// make sure the button was clicked and that there is an array of ids
		if (isset($_POST['btnAction']) AND is_array($_POST['action_to']))
		{
			// pass the ids and let MY_Model delete the items
			$this->simproduct_m->delete_many($this->input->post('action_to'));
		}
		elseif (is_numeric($id))
		{
			// they just clicked the link so we'll delete that one
			$this->simproduct_m->delete($id);
		}
		redirect('admin/simproduct');
	}
	
	/**
	 * Show a  preview
	 * @access	public
	 * @param	int $id The ID of the gallery
	 * @return	void
	 */
	public function preview($id = 0)
	{
		$this->data->item  = $this->simproduct_m->get($id);


		//print_r($this->module_details);
		$this->template->set_layout('modal', 'admin');
		$this->template->title($this->module_details['name'])
						->build('admin/preview', $this->data);
	}
	
}
