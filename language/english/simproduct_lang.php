<?php
//messages
$lang['simproduct:success']			=	'It worked';
$lang['simproduct:error']			=	'It didn\'t work';
$lang['simproduct:no_items']		=	'No Items';

//page titles
$lang['simproduct:create']			=	'Create Item';

//labels
$lang['simproduct:name']			=	'Name';
$lang['simproduct:body']			=	'Body';
$lang['simproduct:image']			=	'Image';
$lang['simproduct:price']			=	'Price';
$lang['simproduct:item_list']		=	'Item List';
$lang['simproduct:view']			=	'View';
$lang['simproduct:edit']			=	'Edit';
$lang['simproduct:delete']			=	'Delete';

//buttons
$lang['simproduct:custom_button']	=	'Custom Button';
$lang['simproduct:items']			=	'Items';
?>
