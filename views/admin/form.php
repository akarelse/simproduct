<section class="title">
	<!-- We'll use $this->method to switch between sample.create & sample.edit -->
	<h4><?php echo lang('simproduct:'.$this->method); ?></h4>
</section>

<section class="item">

	<?php echo form_open_multipart($this->uri->uri_string(), 'class="crud"'); ?>
		
		<div class="form_inputs">
	
		<ul>
			<li class="<?php echo alternator('', 'even'); ?>">
				<label for="name"><?php echo lang('simproduct:name'); ?> <span>*</span></label>
				<div class="input"><?php echo form_input('name', set_value('name', $name), 'class="width-15"'); ?></div>
			</li>

			
			<li class="<?php echo alternator('', 'even'); ?>">
				<label for="body"><?php echo lang('simproduct:content_label'); ?></label>
				<?php echo form_textarea(array('id' => 'body', 'name' => 'body', 'value' => $body, 'rows' => 30, 'class' => 'blog wysiwyg-simple')); ?>
			</li>
			
			
			<li class="<?php echo alternator('', 'even'); ?>">
				<label for="image"><?php echo lang('simproduct:fname'); ?></label>
				<div class="input">
				<?php echo form_dropdown('image_file_id', array(lang('simproduct:no_category_select_label'))+$images) ?>
				</div>
			</li>
			
			<li class="<?php echo alternator('', 'even'); ?>">
				<label for="price"><?php echo lang('simproduct:price'); ?> <span>*</span></label>
				<div class="input"><?php echo form_input('price', set_value('price', $price), 'class="width-15"'); ?></div>
			</li>
		</ul>
		
		</div>
		
		<div class="buttons">
			<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel') )); ?>
		</div>
		
	<?php echo form_close(); ?>

</section>
