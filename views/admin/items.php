<section class="title">
	<h4><?php echo lang('simproduct:item_list'); ?></h4>
</section>

<section class="item">
	<?php echo form_open('admin/simproduct/delete');?>
	
	<?php if (!empty($items)): ?>
	
		<table>
			<thead>
				<tr>
					<th><?php echo form_checkbox(array('name' => 'action_to_all', 'class' => 'check-all'));?></th>
					<th><?php echo lang('simproduct:name'); ?></th>
					<th><?php echo lang('simproduct:body'); ?></th>
					<th><?php echo lang('simproduct:image'); ?></th>
					<th></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="5">
						<div class="inner"><?php $this->load->view('admin/partials/pagination'); ?></div>
					</td>
				</tr>
			</tfoot>
			<tbody>
				<?php foreach( $items as $item ): ?>
				<tr>
					<td><?php echo form_checkbox('action_to[]', $item->id); ?></td>
					<td><?php echo $item->name; ?></td>
					<td><?php echo $item->body; ?></td>
					<td>
					<?php echo img(array('src' => site_url('files/thumb/' . $item->fid), 'alt' => $item->name, 'title' => 'Title: ' . $item->name . ' -- Caption: ' . $item->description)); ?>
					<?php echo form_hidden('action_to[]', $item->id); ?>
					</td>
					<td class="actions">
						<?php echo
						anchor('simproduct', lang('simproduct:view'), 'class="btn green" target="_blank"').' '.
						anchor('admin/simproduct/edit/'.$item->id, lang('simproduct:edit'), 'class="btn orange"'); ?>
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
		
		<div class="table_action_buttons">
			<?php $this->load->view('admin/partials/buttons', array('buttons' => array('delete'))); ?>
		</div>
		
	<?php else: ?>
		<div class="no_data"><?php echo lang('simproduct:no_items'); ?></div>
	<?php endif;?>
	
	<?php echo form_close(); ?>
</section>
